#include "PongAI.hpp"
#include <cmath>

PongAI::PongAI(Paddle& paddle, const Ball& b, unsigned int vision_depth)
    : vision_depth(vision_depth), ball(b), paddle(paddle)
{

}

PongAI::~PongAI()
{

}

void PongAI::Update()
{
    if (std::abs(ball.pos.x - paddle.pos.x) > vision_depth) return; // Don't do anything if the ai can't see the ball, that'd be cheating :)

    if (ball.pos.y < paddle.pos.y)
        paddle.moveUp = true;
    else if (ball.pos.y > paddle.pos.y)
        paddle.moveDown = true;
}
