#include <cmath>
#include "Ball.hpp"
#define Pi = 3.141592;


Ball::Ball()
{
    setStart();
    draw();
}

void Ball::setStart()
{
    size.x = 10;
    size.y = 10;

    pos.x = windowWidth / 2 - size.x / 2;
    pos.y = windowHeight / 2 - size.y / 2;

    dir.x = 2 * (int)((float)rand() / RAND_MAX * 2) - 1;
    dir.y = 0;
    //dir.y = 2 * (int)((float)rand() / RAND_MAX * 2) - 1;
}

void Ball::draw()
{
    sf::RectangleShape ball = sf::RectangleShape(sf::Vector2f(size.x, size.y));
    ball.setFillColor(sf::Color::White);
    ball.setPosition(pos.x, pos.y);

    window.draw(ball);
}

void Ball::moveBall(float deltaT) 
{
    pos.x = pos.x + (ballSpeed * dir.x * deltaT);
    pos.y = pos.y + (ballSpeed * dir.y * deltaT); 
}

int Ball::checkWallCollision()
{
    if (pos.y <= 0 || pos.y + size.y >= windowHeight)
    {
        dir.y = dir.y * -1;
    }

    if (pos.x <= 0 || pos.x + size.x >= windowWidth)
    {
        return 1;
    }
}

void Ball::checkPaddleCollision(double leftPaddlePosY, double rightPaddlePosY, int paddleheight) //fix this to link to paddle::mheight
{
    if (  (pos.y + size.y > leftPaddlePosY && pos.y < (leftPaddlePosY + paddleheight) && pos.x < windowWidth / 2) || (pos.y + size.y > rightPaddlePosY && pos.y < (rightPaddlePosY + paddleheight) && pos.x > windowWidth / 2))
    {
        int side;
        if (pos.x < windowWidth / 2)
        {
            side = 1;
            bouncePaddle(leftPaddlePosY, paddleheight, side);
        }

        if (pos.x > windowWidth / 2)
        {
            side = -1;
            bouncePaddle(rightPaddlePosY, paddleheight, side);
        }
    }
}

void Ball::bouncePaddle(double PaddleY, int paddleheight, int side)
{
    double relativeIntersectY = (PaddleY + (paddleheight / 2)) - pos.y; 
    //relativeIntersect places the middle of the paddle at 0 and the top at 0.5*paddleheight

    double normalizedRelativeIntersectionY = (relativeIntersectY / (paddleheight / 2));
    //Normalize it to make everything independent from paddleheight. Top is now -1, middle = 0, bottom  = -1;
    double bounceAngle = normalizedRelativeIntersectionY * 45;
    //Places the bounceangle between -75* through 0* (perpendicular) to 75*

    dir.x = side * 1 * cos(bounceAngle * 3.141592 / 180.0); 
    //cos and sin functions take radians, also side is needed to reverse the X direction to the right side
    dir.y = -1 * sin(bounceAngle * 3.141592 / 180.0);
}