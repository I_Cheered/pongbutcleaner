#include "Scoresystem.hpp"


int ScoreSystem::scrcP1;
int ScoreSystem::scrcP2;

ScoreSystem::ScoreSystem()
{
    scrcP1 = 0;
    scrcP2 = 0;

    font.loadFromFile("ARCADECLASSIC.TTF");

    ScoreP1text.setFont(font);
    ScoreP1text.setString(std::to_string(scrcP1));
    ScoreP1text.setFillColor(sf::Color::White);
    ScoreP1text.setCharacterSize(100);
    ScoreP1text.setPosition(windowWidth / 4.0, windowHeight / 6.0);
    window.draw(ScoreP1text);

    ScoreP2text.setFont(font);
    ScoreP2text.setString(std::to_string(scrcP2));
    ScoreP2text.setFillColor(sf::Color::White);
    ScoreP2text.setCharacterSize(100);
    ScoreP2text.setPosition(windowWidth / 4.0 * 3.0, windowHeight / 6.0);
    window.draw(ScoreP2text);

    
    goalMessage.setFont(font);
    goalMessage.setString(goalMessagetext);
    goalMessage.setFillColor(sf::Color::White);
    goalMessage.setCharacterSize(60);
    goalMessage.setPosition(windowWidth / 8.0 , windowHeight / 2.0);
    goalMessage.setString("Press  SPACE  to  begin");
    
}

void ScoreSystem::drawScore()
{
    window.draw(ScoreP1text);
    window.draw(ScoreP2text);
}

void ScoreSystem::drawGoalText()
{
    window.draw(goalMessage);
}

void ScoreSystem::goal(double xpos)
{
    if (xpos < (windowWidth / 2))
    {
        scrcP2 = scrcP2 + 1;
        goalMessagetext = "Goal!  Point  awarded  to  player  2";
    }
    else
    {
        scrcP1 = scrcP1 + 1;
        goalMessagetext = "Goal!  Point  awarded  to  player  1";
    }

    ScoreP1text.setString(std::to_string(scrcP1));
    ScoreP2text.setString(std::to_string(scrcP2));
    goalMessage.setString(goalMessagetext);
    drawGoalText();
    drawScore();

    gameState = 1;
}

void ScoreSystem::resetScore()
{
    scrcP1 = 0;
    scrcP2 = 0;

    ScoreP1text.setString(std::to_string(scrcP1));
    ScoreP2text.setString(std::to_string(scrcP2));
    drawScore();
}

void ScoreSystem::checkWin()
{
    if (scrcP1 == 10)
    {
        goalMessage.setString("Winner   is   player   1! \nPress   SPACE   to   restart");
        gameState = 2;
    }
        
    if (scrcP2 == 10)
    {
        goalMessage.setString("Winner   is   player   2! \nPress   SPACE   to   restart");
        gameState = 2;
    }
}