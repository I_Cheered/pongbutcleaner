#include "Paddle.hpp"

//de base constructor wordt altijd eerst gecalld en dan is de derived class nog niet gemaakt dus kan je die functie ook nog niet callen
//Hence gaat ie naar bovenstaande en niet naar onderstaande

Paddle::Paddle()
{
}


Leftpaddle::Leftpaddle()
{
    setLocation();
    upkey = sf::Keyboard::A; 
    downkey = sf::Keyboard::Z;
    draw();
}

Rightpaddle::Rightpaddle()
{
    setLocation();
    upkey = sf::Keyboard::K;
    downkey = sf::Keyboard::M;
    draw();
}


void Leftpaddle::setLocation()
{
    pos.x = offset;
    pos.y = windowHeight / 2 - mheight / 2;
}

void Rightpaddle::setLocation() 
{
    pos.x = windowWidth - mwidth - offset;
    pos.y = windowHeight / 2 - mheight / 2;
}

void Paddle::draw()
{
    sf::RectangleShape player = sf::RectangleShape(sf::Vector2f(mwidth, mheight));
    player.setFillColor(sf::Color::White);
    player.setPosition(pos.x, pos.y);

    window.draw(player);
}

void Paddle::checkPaddleMovement()
{
    if (sf::Keyboard::isKeyPressed(upkey))
        moveUp = true;
    if (sf::Keyboard::isKeyPressed(downkey))
        moveDown = true;
}

void Paddle::movePaddle()
{
    if (moveUp == true)
    {
        pos.y = pos.y - paddlespeed;
        if (pos.y < offset)
            pos.y = offset;
        else if (pos.y > windowHeight - offset - mheight)
            pos.y = windowHeight - offset - mheight;
        moveUp = false;
    }
    if (moveDown == true)
    {
        pos.y = pos.y + paddlespeed;
        if (pos.y < offset)
            pos.y = offset;
        else if (pos.y > windowHeight - offset - mheight)
            pos.y = windowHeight - offset - mheight;
        moveDown = false;
    }
}