#ifndef PONGAI_HPP
#define PONGAI_HPP

#include "Ball.hpp"
#include "Paddle.hpp"

extern const int windowWidth;

class PongAI
{
    private:
        const unsigned int vision_depth;
        const Ball& ball;
        Paddle& paddle;
    protected:
    public:
        PongAI(Paddle& paddle, const Ball& b, unsigned int vision_depth = windowWidth * 0.5); // Constructor
        ~PongAI(); // Destructor

        void Update();
};

#endif
