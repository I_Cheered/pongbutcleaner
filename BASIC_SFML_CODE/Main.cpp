#include <SFML/Graphics.hpp>
#include "Paddle.hpp"
#include "Ball.hpp"
#include "Scoresystem.hpp"
#include "Resources.hpp"
#include "PongAI.hpp"
#include <cmath>

extern const int windowWidth = 1200;
extern const int windowHeight = 800;
sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "Pong but then like cleaner code and shit!");


int gameState = 1;

int deltaT = 1;

int main()
{
    srand(time(NULL));
    window.clear(sf::Color::Black);
    //creating all objects
    Ball ball;
    Leftpaddle leftPaddle;
    Rightpaddle rightPaddle;
    ScoreSystem scoreSystem;

    PongAI ai(leftPaddle, ball);

    window.display();

    sf::Event event;

    while (window.isOpen())
    {
        while (window.pollEvent(event)) //checks if window is being closed
        {
            if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                window.close();
        }
        //leftPaddle.checkPaddleMovement();
        rightPaddle.checkPaddleMovement();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && gameState > 0) //check if game is paused and being unpaused
        {
            leftPaddle.setLocation();
            rightPaddle.setLocation();
            ball.setStart();

            if (gameState == 2)
            {
                scoreSystem.resetScore();
                window.display();
            }
            gameState = 0;
        }

        if (gameState == 0) //if game is not paused, do physics stuffs
        {
            ai.Update();
            ball.moveBall(deltaT);
            leftPaddle.movePaddle();
            rightPaddle.movePaddle();

            int goalint = ball.checkWallCollision();
            if (goalint == 1)
                scoreSystem.goal(ball.pos.x);

            if (std::ceil(ball.pos.x) == leftPaddle.offset + leftPaddle.mwidth || std::ceil(ball.pos.x + ball.size.x) == windowWidth - leftPaddle.offset - leftPaddle.mwidth)
            {
                ball.checkPaddleCollision(leftPaddle.pos.y, rightPaddle.pos.y, Paddle::mheight);
            }
        }
        window.clear(sf::Color::Black);

        ball.draw();
        leftPaddle.draw();
        rightPaddle.draw();
        scoreSystem.drawScore(); 
        if (gameState > 0)
        {
            scoreSystem.checkWin();
            scoreSystem.drawGoalText();
        }
        window.display();
    }
    return 0;
}
