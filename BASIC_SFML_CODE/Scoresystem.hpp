#pragma once
#include <SFML/Graphics.hpp>
#include <string>

extern int gameState;
extern sf::RenderWindow window;
extern const int windowHeight;
extern const int windowWidth;

class ScoreSystem
{
public:
    sf::Font font;
    sf::Text ScoreP1text, ScoreP2text;
    sf::Text goalMessage;
    static int scrcP1, scrcP2;
    std::string goalMessagetext;

    ScoreSystem();

    void drawScore();
    void drawGoalText();

    void goal(double xpos);

    void checkWin();
    void resetScore();

};