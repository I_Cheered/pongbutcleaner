#pragma once
#include <SFML/Graphics.hpp>

#include "Resources.hpp"

extern const int windowHeight;
extern const int windowWidth;
extern sf::RenderWindow window;

class Ball
{
public:
    vec2d pos, dir, size;
    double ballSpeed = 0.5;
    Ball();
    void setStart();
    void moveBall(float deltaT);
    int checkWallCollision();
    void checkPaddleCollision(double leftPaddlePosY, double rightPaddlePosY, int paddleheight);
    void bouncePaddle(double PaddleY, int paddleheight, int side);
    void draw();
};