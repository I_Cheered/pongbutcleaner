#pragma once
#include <SFML/Graphics.hpp>

#include "Resources.hpp"

extern const int windowHeight;
extern const int windowWidth;
extern sf::RenderWindow window;
extern sf::Event event;

class Paddle
{
public:
    vec2d pos, dir;
    const static int mheight = 75;
    const static int mwidth = 5;
    const static int offset = 20;
    sf::Keyboard::Key upkey;
    sf::Keyboard::Key downkey;
    bool moveUp = false, moveDown = false;
    double paddlespeed = 0.3;

    Paddle();
    void checkPaddleMovement();
    void movePaddle();
    void draw();

};

class Leftpaddle : public Paddle
{
public:
    Leftpaddle();
    //Define up and downkey
    void setLocation();
};

class Rightpaddle : public Paddle
{
public:
    Rightpaddle();
    //Define up and downkey
    void setLocation();
};